#!/usr/bin/env sh

set -eu

# Important: This file is to be sourced only. Defines shared functions and variables.
#
# To the extent possible under law, Merlin Diavova <merlindiavova@pm.me>
# has waived all copyright and related or neighboring rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

[ -n "${TRACE+defined}" ] && set -x

CURRENT_STEP=0
readonly VLSS_VERSION=0.8.4
readonly MINIMAL_SYSTEM_TYPE='vlm'
readonly XFCE_SYSTEM_TYPE='xfce'
readonly LUMINA_SYSTEM_TYPE='lumina'
readonly LUMINA_MINIMAL_SYSTEM_TYPE='lumina.minimal'
readonly TRIDENT_SYSTEM_TYPE='trident'
readonly TRIDENT_SERVER_SYSTEM_TYPE='trident.server'
readonly SCRIPT_NAME="$(basename "$0")"
SED_COMMAND='sed -i'

case "$(uname -s)" in
    'Darwin') SED_COMMAND='sed -i .bak' ;;
esac

export BASE_PATH VLSS_VERSION MINIMAL_SYSTEM_TYPE SCRIPT_NAME CURRENT_STEP
export LUMINA_SYSTEM_TYPE LUMINA_MINIMAL_SYSTEM_TYPE SED_COMMAND
export XFCE_SYSTEM_TYPE TRIDENT_SYSTEM_TYPE TRIDENT_SERVER_SYSTEM_TYPE

trap 'die "Interrupted! exiting..."' INT TERM HUP

print_message() {
    [ -n "${VLSS_QUIET:-}" ] && return

    colour_code="${2:-\033[0;37m}";
    printf "${colour_code}%s\n\033[m" "${1}" 2>/dev/null
    unset colour_code
}

die() {
    print_message "FATAL: $*" '\033[0;91m'
    case "$(type cleanup 2>/dev/null)" in
        *function) cleanup
    esac
    exit 1
}

print_warning() {
    [ -n "${VLSS_QUIET:-}" ] && return
    print_message "WARNING: $*" '\033[0;91m'
}

print_error() {
    print_message "ERROR: $*" '\033[0;91m'
}

print_step() {
    [ -n "${VLSS_QUIET:-}" ] && return
    CURRENT_STEP=$((CURRENT_STEP+1))
    print_message "[${CURRENT_STEP}/${STEP_COUNT}] $*"
}

print_info() {
    [ -z "${VLSS_VERBOSE:-}" ] || print_message "[info] $*" '\033[0;96m'
}

print_done() {
    [ -n "${VLSS_QUIET:-}" ] && return
    printf '\n'
    print_message "[Done] $*" '\033[0;32m'
    printf '\n'
}

print_base_usage() {
    about="$1"; shift $(($# ? 1 : 0))
    options=''
    while [ "${1+defined}" ]; do
        options="${options}
    $1"
        shift $(($# ? 1 : 0));
    done

    command printf -- "
$about

Usage: $SCRIPT_NAME [-H <homedirectory>] [-p profile] [-u target_user] [-hqvV]

Options:${options}
    -H <homedirectory>    Set target home diectory
    -p <profile>          Set setup profile name
    -u <username>         Set target username
    -q                    Disable all output
    -h                    Show this help
    -v                    Enable verbose output
    -V                    Show version
"
}

expunge_if_empty() {
    [ -z "$1" ] && die 'No dirctory given to remove'
    [ "$1" = '/' ] && die 'Cannot remove system root'
    [ "$1" = "${BASE_HOME}" ] && die 'Cannot remove home folder'
    [ "$1" = "${HOME}" ] && die 'Cannot remove home folder'
    [ ! -d "$1" ] && print_error 'Folder does not exist' && return 0

    print_info "Will remove ${1} if empty."
    find "${1}" -maxdepth 0 -empty -exec rm -rf {} \;
}

has_binary() {
    command -v "$1" >/dev/null 2>&1
}

assert_is_root() {
    if [ "$(id -u)" -ne 0 ]; then
        die 'Please run as root.'
    fi
}

basename() {
    dir=${1%${1##*[!/]}}
    dir=${dir##*/}
    dir=${dir%"$2"}
    command printf '%s\n' "${dir:-/}"
}

missing_variable() {
    print_error "Please define a $1 in the .env file"
    exit 2
}

assert_required_variables() {
    [ -z "${VLSS_WORKSTATION3_USERNAME:-}" ] \
        && missing_variable 'VLSS_WORKSTATION3_USERNAME'
    [ -z "${VLSS_WORKSTATION3_AUTH_URL:-}" ] \
        && missing_variable 'VLSS_WORKSTATION3_AUTH_URL'
    [ -z "${VLSS_WORKSTATION3_KEY:-}" ] && missing_variable 'VLSS_WORKSTATION3_KEY'
    [ -z "${VLSS_WORKSTATION3_TENANT:-}" ] && missing_variable 'VLSS_WORKSTATION3_TENANT'

    [ -z "${VLSS_TARGET_USER:-}" ] && missing_variable 'VLSS_TARGET_USER'

    for variable; do
        [ -z "${variable:-}" ] && missing_variable "$variable"
    done

    return 0
}

assert_path_exists() {
    if [ ! -e "$1" ]; then
        print_error "Package file [${1}] not found!"
        exit 127
    fi
}

make_executable() {
    chmod 0755 "$1"
    chmod +x "$1"
}

enable_services() {
    for service; do
        if [ ! -e "/var/service/${service}" ] \
            && [ -e "/etc/sv/${service}" ];
        then
            print_info "Enabling service: ${service}"
            ln -s "/etc/sv/${service}" "/var/service/${service}"
        fi
    done
}

disable_services() {
    for service; do
        if [ -e "/var/service/${service}" ];  then
            print_info "Disabling service: ${service}"
            sv stop "/var/service/${service}"

            rm "/var/service/${service}"
        fi
    done
}

down_services() {
    for service; do
        if [ -e "/var/service/${service}" ];  then
            print_info "Disabling and downing service: ${service}"
            sv stop "/var/service/${service}"

            rm "/var/service/${service}"
            touch "/etc/sv/${service}/down"
        fi
    done
}

set_base_home() {
    if [ -n "${VLSS_TARGET_HOME:-}" ]; then
        readonly BASE_HOME="${VLSS_TARGET_HOME}"
    elif [ -d /usr/home ]; then
        readonly BASE_HOME="/usr/home/${VLSS_TARGET_USER}"
    else
        readonly BASE_HOME="/home/${VLSS_TARGET_USER}"
    fi

    [ ! -d "$BASE_HOME" ] && die "Home Directory: ${BASE_HOME} does not exist."

    print_info "Using [$BASE_HOME] as home directory"
    export BASE_HOME
}

write_value() {
    key="$1"
    value="$2"
    file="$3"

    if grep -q "${key}=" "$file"; then
        sed -i "s#\#$key=#$key=#g" "$file"
        sed -i "s#$key=.*#$key=${value}#g" "$file"
    else
        printf '%s\n' "$key=$value" >> "$file"
    fi
    unset key value file
}

parse_base_arguments() {
    while getopts H:p:u:hqvV option; do
        case $option in
            H) export VLSS_TARGET_HOME=$OPTARG ;;
            p) export VLSS_PROFILE=$OPTARG ;;
            u) export VLSS_TARGET_USER=$OPTARG ;;
            h) print_usage; exit 0 ;;
            V) export VLSS_VERBOSE=1 ;;
            q) export VLSS_QUIET=1 ;;
            v) print_message "$SCRIPT_NAME ${SCRIPT_VERSION}"; exit 0 ;;
            *) print_error "Unknown option [$1]"; exit 1 ;;
        esac
        sshift $((OPTIND-1))
    done
}

bootstrap() {
    assert_is_root

    if [ -z "${RUNNING_UNDER_SETUP:-}" ]; then
        [ ! -r "${BASE_PATH}/.env" ] && die 'Cannot read .env file'

        set -o allexport
        # shellcheck disable=SC1090
         \. "${BASE_PATH}/.env"
        set +o allexport

        assert_required_variables
    fi

    [ -z "${VLSS_PROFILE:-}" ] \
        && die 'Please provide a profile in .env or with [-p <profile>]'

    set_base_home
}