#!/usr/bin/env sh
# shellcheck disable=SC1090,SC1091

set -eu

# To the extent possible under law, Merlin Diavova <merlindiavova@pm.me>
# has waived all copyright and related or neighboring rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

export STEP_COUNT=2

if [ -z "${BASE_PATH:-}" ]; then
    a="/$0"; a=${a%/*}; a=${a#/}; a=${a:-.}
    BASE_PATH=$(cd "$a/../" || return; pwd)
    readonly BASE_PATH
    unset a
fi

\. "${BASE_PATH}/scripts/foundation.sh"

print_usage() {
    command printf -- "
Install and setup doas replacing sudo.

Usage: $SCRIPT_NAME [options]

Options:
    -q    Disable all output
    -h    Show this help
    -V    Enable verbose output
    -v    Show version
"
}

main() {
    parse_base_arguments "$@"
    bootstrap

    has_binary doas && STEP_COUNT=$((STEP_COUNT-1))
    ! has_binary sudo && STEP_COUNT=$((STEP_COUNT-1))

    print_step 'Copying etc files'
    copy_etc_files || exit $?

    if ! has_binary doas; then
        print_step 'Installing doas'
        install_doas || exit $?
    fi

    if has_binary sudo; then
        print_step 'Uninstalling doas'
        uninstall_sudo || exit $?
    fi

    print_done 'doas has been installed.'
}

copy_etc_files() {
    cp -f "${BASE_PATH}/overlays/etc/doas.conf" /etc

    sed -i "s/:username/:${VLSS_TARGET_USER}/g" /etc/doas.conf

    chmod 544 /etc/doas.conf
}

install_doas() {
    xbps-install -Sy opendoas
}

uninstall_sudo() {
    xbps-remove -y sudo
    xbps-remove -oO
}

main "$@"