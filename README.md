# Void Linux System Setup
Programmatic instructions to setup a baseline [Void Linux](https://voidlinux.org) installation. It is generally best to run this right after a fresh install.

## For here be growing dragons
This setup may not be suitable for everybody.

Please read and inspect everything before copying and/or using the files found here.

## Pre-flight

### Git
Ensure `git` is installed.

```sh
  # xbps-install -S git
```
Clone this repository.
```sh
  $ git clone https://gitlab.com/merlindiavova/void-linux-system-setup vlss
```

### Provide settings
Copy `.env.example` to `./env` and enter the correct settings for the system setup.

> **Please do not skip this step**

| KEY                        | NOTES                      |
| -------------------------- | -------------------------- |
| VLSS_TARGET_USER           | *Required*. The desired username on the target system. |
| VLSS_TARGET_HOME           | *Required*. The user home directory path on the target system. |
| VLSS_PROFILE               | *Required*. The package profile. Generally rule is to name it your OS distribution i.e 'xfce'. |
| VLSS_LOCALE                | *Required*. System locale. |
| VLSS_HARDWARE_CLOCK        | *Required*. System clock UTC or local. |
| VLSS_TIMEZONE              | *Required*. System Timezone. |
| VLSS_KEYMAP                | *Required*. System Keyboard layout. |
| VLSS_ENCODING              | *Required*. System Encoding (ignored for musl version). |
| VLSS_LANGUAGE              | *Required*. System language (ignored for musl version). |
| VLSS_CORE_SERVICES        | *Required*. The core services that should be started. |
| VLSS_SERVICES             | *Required*. Other services that you want to start. |
| VLSS_CPU_TYPE              | *Required*. Your CPU type in lowercase, i.e intel, amd. |
| VLSS_BOOT_TYPE             | *Required*. EFI or MBR. Only provided if the system was not installed via my VLI package. |
| VLSS_WINDOW_MANAGER_PACKAGES | Optional. Only set this if you intend on using a custom WM setup.. |
| VLSS_WORKSTATION3_USERNAME | Only for me, can be ignored by others. |
| VLSS_WORKSTATION3_KEY      | Only for me, can be ignored by others. |
| VLSS_WORKSTATION3_AUTH_URL | Only for me, can be ignored by others. |
| VLSS_WORKSTATION3_TENANT   | Only for me, can be ignored by others. |
| VLSS_WEBDAV_URL            | Optional. Public URL for WebDAV. |
| VLSS_WEBDAV_USERNAME       | Optional. WebDAV username. |
| VLSS_WEBDAV_PASSWORD       | Optional. WebDAV password. |
| VLSS_WEBDAV_MOUNT_DIR      | Optional. Mount directory for WebDAV. |
| VLSS_TRANSMISSION_SERVER   | Optional. Transmission server IP/hostname. |
| VLSS_TRANSMISSION_PORT     | Optional. Tranmission port number, only if different to 9091. |
| VLSS_WITH_DOWNLOAD_TMPFS   | Optional. Setup downloads directory as tmpfs. |
| VLSS_WITH_RCLONE           | Optional. Install rclone. |
| VLSS_WITH_LENOVO_FIX       | Optional. Install [throttled](https://github.com/erpalma/throttled). Please check system support before setting this. |
| VLSS_WITH_DOAS             | Optional. Replace sudo with doas. |
| VLSS_WITH_EXTRA_THEMES     | Optional. Install extra GTK and icons themes. Currently install Pop!_OS dark themes. |
| VLSS_WITH_ZZZ              | Optional. Install and setup zzz hooks. |
| VLSS_VERBOSE               | Optional. Verbose output during setup. |
| VLSS_USE_SCP               | Optional. Use scp instead of rsync for file transfers. |
| VLSS_REMOVE_SERVICES      | Optional. Remove any undesired services. |

### Push
I find it easier to setup a workstation via SSH so I can copy and paste commands. I clone the repository to another computer first and then push it to my target machine. To make it easier there is the `./scripts/push` shellscript.

```sh
  $ ./setup-void push <remote-ip> [remote-username] [remote-path]
```

> `[remote-path]` is optional. If omitted the `$VLSS_TARGET_HOME` env variable will be used.
> `[remote-username]` is optional. If omitted the `$VLSS_TARGET_USERNAME` env variable will be used.

### Project Trident
If the target machine
 - is a Project Trident install
 - or has `nftables` installed
 - and was not installed from [Void Linux Encrypted Install](https://gitlab.com/merlindiavova/void-linux-encrypted-install) (VLI) package

 we need to open the SSH port.

Copy the sample file.
```sh
  # cd /etc/firewall-conf
  # cp custom-input.conf.sample custom-input.conf
```

Append the SSH rules to the copied file.
```sh
  # printf 'add rule inet filter input tcp dport %d accept\n' 22 >> custom-input.conf
```

Restart the firewall.
```sh
  # sv restart nftables
```

Start the sshd service.
```sh
  # ln -s /etc/sv/sshd /var/service
```

## Usage

```sh
  # ./setup-void
```

The above command will run
 - `./scripts/prepare-system` -  Performs initial prep on a base Void installation.
 - `./scripts/install-packages` - Install and configure system packages.
 - `./scripts/pull-assets` - Pull private assets and credentials from workstation3. (Only applies to me. Ignored for everybody else).
 - `./scripts/setup-runsvdir` - Setup and confgure user runit services.
 - `./scripts/setup-zzz` -  If `VLSS_WITH_ZZZ` is set, setup `zzz` hooks and user scripts.
 - `./scripts/install-doas` - If `VLSS_WITH_DOAS` is set, install and setup doas replacing sudo.
 - `./scripts/install-edns` - Install and configure Encrypted DNS.
 - `./scripts/install-wm` - If `VLSS_WINDOW_MANAGER_PACKAGES` is set, install the given window manager packages.
 - `./scripts/build-themes` - If `VLSS_WITH_EXTRA_THEMES` is set, build and install Pop!\_OS GTK and Icon theme.
 - `./scripts/finalize-setup` - Performs final steps to close the setup process.
 - `./scripts/setup-download-tmpfs` - If `VLSS_WITH_DOWNLOAD_TMPFS` is set, setup download directory as a tmpfs.
  - `./scripts/setup-davfs` - If `VLSS_WITH_DAVFS` is set, setup a DAV drive using the `VLSS_WEBDAV_*` variables

You can run all these scripts individually. However please note the order.
`./scripts/pull-assets`, `./scripts/build-themes`, `./scripts/setup-download-tmpfs` and `./scripts/setup-davfs`  are not required.

## Help

```sh
 Wrapper around the other setup scripts.
 Options are read from the .env file. See .env.example

 Usage: $SCRIPT_NAME [options] [command] [options]

 Options:
     -h    Show this help
     -V    Show version

 Available commands:
    assets        Pull private assets and credentials from workstation3.
    davfs         Setup davfs, configure fstab and secrets.
    doas          Install and setup doas, replacing sudo.
    downloadfs    Setup downloads folder as tmpfs.
    edns          Install and configure Encrypted DNS (DNSMasq and DNSCrypt-Proxy).
    finalize      Performs final steps to close the setup process.
    packages      Install packages defined in ./packages files.
    prepare       Performs initial prep on a base Void installation.
    push          rsync and scp wrapper to push this package to a remote system.
    runsvdir      Setup and confgure user runit services.
    themes        Build and install GTK and Icon themes.
    zzz           Setup zzz hooks and user scripts.
    wm            Install a window manager and given packages.
```

## Packages
This script comes with some base packages installed via the `xbps package manager` and some other packages installed via curl download.

Please see `packages/base.packages` and `packages/base.curl-packages` to see which base packages will be downloaded and installed.

### Define extra packages
If you want to define extra packages create `packages/<profile-name>.packages` and/or `packages/<profile-name>.curl-packages` and assign `VLSS_PROFILE=<profile-name>` in your .env file.
When you invoke `./setup-void` or `./setup-void packages`, the defined packages will be downlaoded and installed.

## Information
Usergroups are `wheel,audio,video,floppy,cdrom,optical,kvm,xbuilder,docker,socklog`

## Notes
Trialing `xwallpaper`, if it works then removing `feh` is recommended.
Trialing `pcmanfm`, if I like it then remove `spacefm`.

## Troubleshooting

### Docker Service
Depending on the system configuraton, the docker service may fail to start or remain stable. Edit the `/etc/sv/docker/run` and comments out the `[ -n "$OPTS" ] || OPTS="s=overlay2` line.

## Testing

```sh
  $ ./scripts/lint
```

## Copying
Void Linux System Setup (this package) is in the public domain.

To the extent possible under law, Merlin Diavova merlindiavova@pm.me has waived all copyright and related or neighboring rights to this work.

http://creativecommons.org/publicdomain/zero/1.0/