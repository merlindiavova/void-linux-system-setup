#!/usr/bin/env sh

# To the extent possible under law, Merlin Diavova <merlindiavova@pm.me>
# has waived all copyright and related or neighboring rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

# Heavily inpsired by from https://github.com/bahamas10/zzz-user-hooks

set -euf

[ "${TRACE+defined}" ] && set -x

OPTION_TIMEOUT='5s'
SESSION_DISPLAY=''
REGEX_DISPLAY_NUMBER='^[0-9]+$'
USER_HOME_DIRECTORY=''
USER_USERNAME=''
ACTION=''

print_line() {
  command echo '[user-script]     ' "$@"
}

print_error_line() {
  command echo '[user-script][error]     ' "$@" >&2
  exit 1
}

x_session_display() {
  for sock in /tmp/.X11-unix/X*;
  do
    # socket group is the user that owns the session
    user=$(stat --printf '%G' "$sock")
    [ -n "$user" ] || print_error_line "failed to find owner (group) of $sock"

    USER_USERNAME="$user"
    unset user
    # extract the display number
    display=${sock##*/}
    display=${display#X}

    printf '%s' "$display" | grep -Eq "$REGEX_DISPLAY_NUMBER" || \
      print_error_line "invalid display number found: '$display'"

    SESSION_DISPLAY="$display"
    unset display sock
    return 0
    break
  done

  return 1
}

assert_file_is_executable() {
  [ -x "$1" ] || print_error_line "script $1 not found or not executable"
}

assert_xauthority_file_exists() {
  [ -e "${USER_HOME_DIRECTORY}/.Xauthority" ] || \
    print_error_line "failed to find Xauthority file for user $USER_USERNAME"
}

execute_suspend() {
  readonly ACTION_SCRIPT=/opt/zzz/hooks/on_suspend
  assert_file_is_executable "$ACTION_SCRIPT"
  assert_xauthority_file_exists
  env="DISPLAY=:$SESSION_DISPLAY XAUTHORITY=${USER_HOME_DIRECTORY}/.Xauthority"

  print_line "Running $ACTION_SCRIPT for user $USER_USERNAME with $env"
  # We want word splitting here
  # shellcheck disable=SC2086
  doas -u "$USER_USERNAME" $env timeout "$OPTION_TIMEOUT" "$ACTION_SCRIPT"
  print_line "ran $ACTION_SCRIPT for user $USER_USERNAME, exited $?"
}

execute_resume() {
  readonly ACTION_SCRIPT=/opt/zzz/hooks/on_resume
  assert_file_is_executable "$ACTION_SCRIPT"
  assert_xauthority_file_exists
  env="DISPLAY=:$SESSION_DISPLAY XAUTHORITY=${USER_HOME_DIRECTORY}/.Xauthority"

  print_line "Running $ACTION_SCRIPT for user $USER_USERNAME with $env"
  # We want word splitting here
  # shellcheck disable=SC2086
  doas -u "$USER_USERNAME" $env timeout "$OPTION_TIMEOUT" "$ACTION_SCRIPT"
  print_line "ran $ACTION_SCRIPT for user $USER_USERNAME, exited $?"
}

parse_arguments() {
  [ ! "${1+defined}" ] && { command echo 'Whoops'; exit 20; }

  while [ "${1+defined}" ]; do
    case "$1" in
      suspend) readonly ACTION='execute_suspend' ;;
      resume) readonly ACTION='execute_resume' ;;
      *) print_error_line "Invalid mode [$1] given."
    esac
    shift "$(($# ? 1 : 0))"
  done

  if [ ! "${ACTION+defined}" ];
  then
    command echo "Nothing set"
    exit 10
  fi
}

main() {
  parse_arguments "$@"

  x_session_display || print_error_line 'Cannot find Active Session'

  readonly USER_HOME_DIRECTORY=$(getent passwd "$USER_USERNAME" | cut -d: -f6)

  if [ -z "$USER_HOME_DIRECTORY" ] || [ ! -d "$USER_HOME_DIRECTORY" ];
  then
    print_error_line "failed to find user $USER_USERNAME home directory"
  fi

  $ACTION
}

main "$@"
