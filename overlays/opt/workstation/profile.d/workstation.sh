# Workstation profile init

trap 'wkstn_logout; exit' 0

username="$(id -nu)"

runas /opt/workstation/bin/login "${username}"

wkstn_logout() {
	runas /opt/workstation/bin/logout "${username}"
}
